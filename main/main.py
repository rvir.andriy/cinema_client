import pandas as pd
import requests
import json
import sys


from pandas.io.json import json_normalize

def check_connectivity(reference):
    try:
        _ = requests.get(url, timeout=10)
        return True
    except requests.ConnectionError:
        print("No connection to "+reference)
    return False

def getMovies(url):
    resp = requests.get(url)
    print("Status:  " + str(resp.status_code))
    if(resp.status_code!=200):
        print(resp.text)
        return
    df = pd.read_json(resp.text)
    print('id\ttime\t\t\t\t\t\tmovie\tgenre\tprice\thall_id')
    for movie in df['Schedule']:
        print(str(movie['id'])+'\t'+movie['time']+'\t'+movie['movie']+'\t'+movie['genre']+'\t'+str(movie['price'])+'\t\t'+str(movie['hall_id']))

def getMovieByID(url, id):
    resp = requests.get(url+'/movie/'+str(id))
    print("Status:  " + str(resp.status_code))
    if(resp.status_code!=200):
        print(resp.text)
        return
    print('Movie, where id=' + str(id))
    js=json.loads(resp.text)
    print('id\tname\tgenre')
    print(str(js['id']) + '\t' + js['name'] + '\t' + js['genre'])
    return js

def getSceduleByGenre(url, genre):
    resp = requests.get(url+'/genre/'+genre)
    print("Status:  " + str(resp.status_code))
    if(resp.status_code!=200):
        print(resp.text)
        return
    df = pd.read_json(resp.text)
    print('id\tTime\t\t\t\tMovie\tGenre\tPrice\tHall')
    for schedule in df['schedule']:
        print(str(schedule['id']) + '\t' + schedule['time'] + '\t' + schedule['movie'] + '\t' + schedule['genre'] + '\t' + str(schedule['price']) + '\t' + str(schedule['hall_id']))

def deleteMovie(url, id):
    resp = requests.delete(url+'/delete/'+str(id))
    print("Status:  " + str(resp.status_code))
    print("Result:  " + str(resp.text))

def postAddSchedule(url, js):
    headers={'Content-type': 'application/json'}
    resp = requests.post(url+'/schedule',data=json.dumps(js),headers=headers)
    print("Status:  " + str(resp.status_code))
    print("Result:  " + str(resp.text))

if __name__ == "__main__":
    url = 'http://localhost:9000'
    if(check_connectivity(url)):
        print('Connected to '+url)
    else:
        print('exit')
        sys.exit(1)

    print('**************************************\nAll Movies:')
    getMovies(url)
    print('**************************************')
    movieId = 1
    print('Movie, where id='+ str(movieId))
    getMovieByID(url, movieId)
    print('**************************************')
    genre = 'Action'
    print('Scedule, where genre=' + genre)
    getSceduleByGenre(url,genre)
    print('**************************************')
    delId = 101
    print('Delete Movie, where id=' + str(delId))
    deleteMovie(url,delId)
    print('**************************************')
    js = [{'id':1,'time':'2018-05-25 11:30:00.000','movie':'Movie1','genre':'Action','price':50.00,'hall_id':1},
{'id': 2, 'time': '2018-05-25 13:30:00.000', 'movie': 'Movie2', 'genre': 'Comedy', 'price': 50.00,
          'hall_id': 1},
{'id': 3, 'time': '2018-05-25 15:30:00.000', 'movie': 'Movie3', 'genre': 'Adventure', 'price': 50.00,
          'hall_id': 1},
{'id': 4, 'time': '2018-05-25 17:30:00.000', 'movie': 'Movie4', 'genre': 'Horror', 'price': 50.00,
          'hall_id': 1},
{'id': 5, 'time': '2018-05-25 20:30:00.000', 'movie': 'Movie5', 'genre': 'Adventure', 'price': 50.00,
          'hall_id': 1},
{'id': 6, 'time': '2018-05-25 22:30:00.000', 'movie': 'Movie6', 'genre': 'Action', 'price': 50.00,
          'hall_id': 1},
{'id': 4, 'time': '2018-05-25 17:30:00.000', 'movie': 'Movie4', 'genre': 'Horror', 'price': 50.00,
          'hall_id': 1},
{'id': 7, 'time': '2018-05-25 20:30:00.000', 'movie': 'Movie1', 'genre': 'Action', 'price': 50.00,
'hall_id': 2},
{'id': 8, 'time': '2018-05-25 18:30:00.000', 'movie': 'Movie2', 'genre': 'Comedy', 'price': 50.00,
'hall_id': 2},
{'id': 9, 'time': '2018-05-25 16:30:00.000', 'movie': 'Movie3', 'genre': 'Adventure', 'price': 50.00,
'hall_id': 2},
{'id': 10, 'time': '2018-05-25 14:30:00.000', 'movie': 'Movie4', 'genre': 'Horror', 'price': 50.00,
'hall_id': 2},
{'id': 11, 'time': '2018-05-25 12:30:00.000', 'movie': 'Movie5', 'genre': 'Adventure', 'price': 50.00,
'hall_id': 2},
{'id': 12, 'time': '2018-05-25 10:30:00.000', 'movie': 'Movie6', 'genre': 'Action', 'price': 50.00,
'hall_id': 2} ]
    print('Create new Schedules')
    for val in js:
        postAddSchedule(url,val)
    print('**************************************')







